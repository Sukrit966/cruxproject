package com.sukrit.cruxproject.helper;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;

import androidx.constraintlayout.widget.ConstraintLayout;

public class SwipeDetectorLayout extends ConstraintLayout {
    private float x1,x2;
    static final int MIN_DISTANCE=300;
    private onSwipeEventDetected mSwipeDetectedListener;

    public SwipeDetectorLayout(Context context) {
        super(context);
    }

    public SwipeDetectorLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public SwipeDetectorLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {

        switch(ev.getAction())
        {
            case MotionEvent.ACTION_DOWN:
                x1 = ev.getX();
                break;
            case MotionEvent.ACTION_UP:
                x2 = ev.getX();
                float deltaX = x2 - x1;
                if (Math.abs(deltaX) > MIN_DISTANCE)
                {
                    if(deltaX<0)
                    {
                        if(mSwipeDetectedListener!=null)
                            mSwipeDetectedListener.swipeEventDetected(1);
                    }else{
                        if(mSwipeDetectedListener!=null)
                            mSwipeDetectedListener.swipeEventDetected(2);
                    }
                }
                break;
        }
        return super.onInterceptTouchEvent(ev);
    }

    public interface onSwipeEventDetected
    {
        void swipeEventDetected(int TYPE);
    }

    public void setSwipeEventsListener(onSwipeEventDetected listener)
    {
        this.mSwipeDetectedListener=listener;
    }
}
