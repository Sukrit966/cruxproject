package com.sukrit.cruxproject.helper;

import android.content.Context;

import com.google.gson.Gson;
import com.orhanobut.hawk.Hawk;
import com.sukrit.cruxproject.model.Favourite;
import com.sukrit.cruxproject.model.XkcdResponse;

import java.util.ArrayList;
import java.util.List;

public class PreferenceHelper {

    Gson gson;
    private String FAVOURITE_COMIC = "favourite";
    private String favourite =null;


    int current;

    public PreferenceHelper(Context mContext) {
        Hawk.init(mContext).build();
        gson = new Gson();
        if (Hawk.count() != 0L) {
            favourite = Hawk.get(FAVOURITE_COMIC, "");
        } else {
            favourite = "";
        }
    }

    public int getCurrent(){
        if(Hawk.count() != 0){
            return Hawk.get("current_item",42);
        }else{
            return 42;
        }
    }

    public void setCurrent(int current){
        Hawk.put("current_item",current);
    }

    public String getFavourite() {
        favourite = Hawk.get(FAVOURITE_COMIC,"");
        return favourite;
    }

    public List<String> getFavNum(){
        List<String> num = new ArrayList<>();
        List<XkcdResponse> favouriteList;
        if(favourite != null && favourite.length() > 0 ){
            Favourite fav = gson.fromJson(favourite,Favourite.class);
            favouriteList = fav.getFavouriteComics();
            for(XkcdResponse f : favouriteList){
                num.add(f.getNum());
            }
        }
        return num;
    }

    public void addComic(XkcdResponse xkcdResponse){
        List<XkcdResponse> favouriteList;
        int flag = 0;
        if(favourite != null && favourite.length() > 0 ){
            com.sukrit.cruxproject.model.Favourite fav = gson.fromJson(favourite, Favourite.class);
            favouriteList = fav.getFavouriteComics();
            for (XkcdResponse f : favouriteList) {
                if (f.getTitle() == xkcdResponse.getTitle()) {
                    flag = 1;
                }
            }

            if(flag == 0){
                favouriteList.add(xkcdResponse);
            }

            fav.setFavouriteComics(favouriteList);
            favourite = gson.toJson(fav);
            Hawk.put(FAVOURITE_COMIC,favourite);
        }else{
            favouriteList = new ArrayList<>();
            favouriteList.add(xkcdResponse);
            Favourite fav = new Favourite(favouriteList);
            favourite = gson.toJson(fav);
            Hawk.put(FAVOURITE_COMIC,favourite);
        }
    }

    public void removeComic(String name){
        List<XkcdResponse> favouriteList;
        List<XkcdResponse> newList = new ArrayList<XkcdResponse>();
        favourite = Hawk.get(FAVOURITE_COMIC,"");
        Favourite fav = gson.fromJson(favourite,Favourite.class);
        favouriteList = fav.getFavouriteComics();
        int flag = 0 ;

        for(XkcdResponse f : favouriteList){
            if(!f.getTitle().equals(name)){
                newList.add(f);
            }
        }

        fav.setFavouriteComics(newList);
        String output = gson.toJson(fav);
        favourite = output;
        Hawk.put(FAVOURITE_COMIC,favourite);

    }
}
