package com.sukrit.cruxproject.helper;

public interface ClickListener {
    void onClicked(int position);
}