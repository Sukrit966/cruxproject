package com.sukrit.cruxproject.adapter;

import android.content.Context;
import android.view.HapticFeedbackConstants;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.sukrit.cruxproject.R;
import com.sukrit.cruxproject.helper.ClickListener;
import com.sukrit.cruxproject.model.XkcdResponse;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FavouriteAdapter extends RecyclerView.Adapter<FavouriteAdapter.FavouriteViewHolder>{


    List<XkcdResponse> favouriteComicList;
    Context mContext;
    ClickListener listener;

    public FavouriteAdapter(List<XkcdResponse> favouriteComicList, Context mContext, ClickListener listner) {
        this.favouriteComicList = favouriteComicList;
        this.mContext = mContext;
        this.listener = listner;
    }


    @NonNull
    @Override
    public FavouriteViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_favourite, parent, false);
        return new FavouriteViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull FavouriteViewHolder holder, int position) {
        XkcdResponse xkcdResponse = favouriteComicList.get(position);
        holder.topicName.setText(xkcdResponse.getTitle());
        holder.subtopicName.setText("#" + xkcdResponse.getNum());
        Glide.with(mContext)
                .load(xkcdResponse.getImg())
                .into(holder.iconImage);

        holder.mainContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onClicked(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return favouriteComicList.size();
    }

    public void setFavouriteComicList(List<XkcdResponse> favouriteComicList) {
        this.favouriteComicList = favouriteComicList;
    }

    public class FavouriteViewHolder extends RecyclerView.ViewHolder implements View.OnLongClickListener {
        @BindView(R.id.topicName)
        TextView topicName;
        @BindView(R.id.subtopicName)
        TextView subtopicName;
        @BindView(R.id.icon_image)
        ImageView iconImage;
        @BindView(R.id.icon_container)
        RelativeLayout iconContainer;
        @BindView(R.id.list_row)
        RelativeLayout mainContainer;

        public FavouriteViewHolder(View itemView) {
            super(itemView);
            itemView.setOnLongClickListener(this);
            ButterKnife.bind(this, itemView);
        }

        @Override
        public boolean onLongClick(View view) {
           // listener.onLongClicked(getAdapterPosition());
            view.performHapticFeedback(HapticFeedbackConstants.LONG_PRESS);
            return true;
        }
    }
}
