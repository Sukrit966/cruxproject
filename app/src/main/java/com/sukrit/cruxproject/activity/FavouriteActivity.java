package com.sukrit.cruxproject.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.google.gson.Gson;
import com.sukrit.cruxproject.R;
import com.sukrit.cruxproject.adapter.FavouriteAdapter;
import com.sukrit.cruxproject.helper.ClickListener;
import com.sukrit.cruxproject.helper.PreferenceHelper;
import com.sukrit.cruxproject.model.Favourite;
import com.sukrit.cruxproject.model.XkcdResponse;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FavouriteActivity extends AppCompatActivity {

    @BindView(R.id.recyclerViewFavourite)
    RecyclerView recyclerView;

    @BindView(R.id.textView)
    TextView textView;

    Gson gson;
    PreferenceHelper preferenceHelper;
    List<XkcdResponse> comicList;
    FavouriteAdapter adapter;
    RecyclerView.LayoutManager layoutManager;
    String favString;
    Favourite favourite;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favourite);
        ButterKnife.bind(this);
        gson = new Gson();
        preferenceHelper = new PreferenceHelper(this);
        favString = preferenceHelper.getFavourite();
        favourite = gson.fromJson(favString,Favourite.class);
        if(favourite != null){
            comicList = favourite.getFavouriteComics();
        }else {
            comicList = new ArrayList<>();
        }
        Collections.reverse(comicList);
        adapter = new FavouriteAdapter(comicList, this, new ClickListener() {
            @Override
            public void onClicked(int position) {
                XkcdResponse xkcdResponse = comicList.get(position);
                Intent intent = new Intent();
                intent.putExtra("COMIC_NUM",xkcdResponse.getNum());
                setResult(RESULT_OK,intent);
                finish();
            }
        });

        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle("Favourite Comics");
        }
        layoutManager = new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
        updateScreen();
    }

    void updateData(){
        String favString = preferenceHelper.getFavourite();
        Favourite favourite = gson.fromJson(favString,Favourite.class);
        if(favourite != null){
            comicList = favourite.getFavouriteComics();
        }else {
            comicList = new ArrayList<>();
        }
        Collections.reverse(comicList);
        adapter.setFavouriteComicList(comicList);
        adapter.notifyDataSetChanged();
    }

    @Override
    protected void onResume() {
        updateData();
        updateScreen();
        super.onResume();
    }

    private void updateScreen() {
        if (comicList.isEmpty()) {
            textView.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.INVISIBLE);
        } else {
            textView.setVisibility(View.INVISIBLE);
            recyclerView.setVisibility(View.VISIBLE);
        }
    }
}
