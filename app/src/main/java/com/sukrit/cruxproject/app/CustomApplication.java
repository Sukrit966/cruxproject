package com.sukrit.cruxproject.app;

import android.app.Application;
import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.sukrit.cruxproject.model.XkcdResponse;
import com.sukrit.cruxproject.service.XkcdAPI;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class CustomApplication extends Application {

    Retrofit retrofit;
    XkcdAPI xkcdAPI;

    String BASE_URL = "https://xkcd.com/";
    int MAX_COMIC = 2202;

    @Override
    public void onCreate() {
        super.onCreate();
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        xkcdAPI = retrofit.create(XkcdAPI.class);
        updateMax();
    }

    void updateMax(){
        Call<XkcdResponse> call = xkcdAPI.getHome();
        call.enqueue(new Callback<XkcdResponse>() {
            @Override
            public void onResponse(Call<XkcdResponse> call, Response<XkcdResponse> response) {
                if(response.isSuccessful()){
                    if(response.body() != null){
                        MAX_COMIC = Integer.parseInt(response.body().getNum());
                    }
                }
            }

            @Override
            public void onFailure(Call<XkcdResponse> call, Throwable t) {
                MAX_COMIC = 0;
            }
        });
    }

    public int getMAX_COMIC(){
        return MAX_COMIC;
    }


    public static CustomApplication get(Context context) {
        return (CustomApplication) context.getApplicationContext();
    }

    public XkcdAPI xkcdAPI() {
        return xkcdAPI;
    }

    public Retrofit retrofit() {
        return retrofit;
    }
}
