package com.sukrit.cruxproject.model;

import java.util.List;

public class Favourite {

    List<XkcdResponse> favouriteComics;

    public Favourite(List<XkcdResponse> favouriteComics) {
        this.favouriteComics = favouriteComics;
    }

    public List<XkcdResponse> getFavouriteComics() {
        return favouriteComics;
    }

    public void setFavouriteComics(List<XkcdResponse> favouriteComics) {
        this.favouriteComics = favouriteComics;
    }

}
