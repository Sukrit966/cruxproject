package com.sukrit.cruxproject.service;

import com.sukrit.cruxproject.model.XkcdResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface XkcdAPI {

    @GET("{number}/info.0.json")
    Call<XkcdResponse> getSpecific(@Path("number") int num);

    @GET("info.0.json")
    Call<XkcdResponse> getHome();
}
